import React, { Component } from 'react';
import { Input } from 'antd';
const Search = Input.Search;

class SearchBox extends Component {
  constructor(props) {
    super(props);

    this.state = {
      search: ''
    }

    this.handleSearch = this.handleSearch.bind(this);
  }

  handleSearch(event) {
    console.log(event.target.value);
    this.setState({
      search: event.target.value
    })
  }

  render() {
    return(
      <div>
        <Search
          placeholder="Search Item"
          onKeyUp={this.search}
        />
        <br /><br />
      </div>
    );
  }
};

export default SearchBox;
