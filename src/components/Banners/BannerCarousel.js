import React, { Component } from 'react';
import Slider from 'react-slick';
import './Banner.css';

class BannerCarousel extends Component {
  render() {
    const style = {
      margin: '25px'
    };
    const settings = {
      arrows: true,
      dots: true,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
      autoplay: true,
      autoplaySpeed: 5000
    };
    return(
      <div style={style}>
        <Slider {...settings}>
          <div className='banner'><img alt='sample' src='https://spark.adobe.com/images/landing/examples/hiking-fb-cover.jpg' width="8alt='sample' 00"/></div>
          <div className='banner'><img alt='sample' src='https://spark.adobe.com/images/landing/examples/hiking-fb-cover.jpg' width="100%"/></div>
          <div className='banner'><img alt='sample' src='https://spark.adobe.com/images/landing/examples/hiking-fb-cover.jpg'/></div>
        </Slider>
      </div>
    );
  }
}

export default BannerCarousel;
