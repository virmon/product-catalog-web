import React, { Component } from 'react';
import { Layout, Menu, Icon } from 'antd';
import { BrowserRouter as Router, Route, Link, Switch } from 'react-router-dom';

// import components
import AppHeader from '../base_layout/AppHeader';
import AppBreadcrumb from '../base_layout/AppBreadcrumb';
import AppFooter from '../base_layout/AppFooter';
import ProductList from '../Products/ProductList';
import Bags from '../Products/Bags';
import Makeups from '../Products/Makeups';
import ProductDetail from '../Products/ProductDetail';
import ModeOfPayment from '../Static/ModeOfPayment';
import CategoryList from '../Categories/CategoryList';
import ScrollToTop from '../ScrollToTop';
import ScrollButton from '../ScrollButton';
import '../ScrollButton.css';
import NoMatch from '../NoMatch/NoMatch';

const { Content, Sider } = Layout;
const SubMenu = Menu.SubMenu;

const menus = module.export = [
  {
    key: '1',
    name: 'Home',
    path: '/',
    icon: 'home',
    click: true,
  },
  {
      key: '2',
      name: 'Categories',
      icon: 'profile',
      click: false,
      child: [
        {
          key: '2.1',
          name: 'All',
          path: '/catalog'
        },
        {
          key: '2.2',
          name: 'Bags',
          path: '/bags'
        },
        {
          key: '2.3',
          name: 'Makeup',
          path: '/makeups'
        }
      ]
  },
  {
      key: '3',
      name: 'How to pay',
      path: '/mode_of_payment',
      icon: 'info-circle-o',
      click: true,
  }
];

class AppLayout extends Component {
  state = {
    collapsed: false
  };

  onCollapse = (collapsed) => {
    // console.log(collapsed);
    this.setState({ collapsed });
  }

  getSiderMenu(key, name, icon, path, click, child) {
      if(!click){
        return(
            <SubMenu
              key={key}
              title={<span><Icon type={icon} /><span>{name}</span></span>}
            >
            {
              child.map((item) =>
                <Menu.Item key={item.key}>
                  <Link exact to={item.path} >
                    {item.name}
                  </Link>
                </Menu.Item>
              )
            }
            </SubMenu>
        );
        } else {
          return(
              <Menu.Item key={key}>
                <Link exact to={path} >
                <Icon type={icon} />
                <span>{name}</span>
                </Link>
              </Menu.Item>
          );
        }
  }
  render() {
    return (
      <Router>
      <ScrollToTop>
      <Layout>
        <AppHeader />
        <Layout>
          <Sider
            breakpoint="lg"
            collapsedWidth={0}
            collapsible
            trigger
            collapsed={this.state.collapsed}
            onCollapse={this.onCollapse}
            >
            <div className="logo" />
            <Menu theme="dark" mode="inline" defaultSelectedKeys={['1']}>
              {
                menus.map((menu) =>
                  this.getSiderMenu(menu.key, menu.name, menu.icon, menu.path, menu.click, menu.child)
                )
              }
            </Menu>
          </Sider>
          <Layout style={{ padding: '0 24px 24px' }}>
            <AppBreadcrumb />
            <Content style={{ background: '#fff', padding: 24, margin: 0, minHeight: 700 }}>
              <Switch>
                  <Route exact path="/" component={CategoryList}/>
                  <Route exact path="/catalog" component={ProductList}/>
                  <Route exact path="/bags" component={Bags}/>
                  <Route path="/bags/:id" component={ProductDetail} />
                  <Route exact path="/makeups" component={Makeups}/>
                  <Route path="/makeups/:id" component={ProductDetail} />
                  <Route path="/mode_of_payment" component={ModeOfPayment}/>
                  <Route component={NoMatch}/>
              </Switch>
              <ScrollButton scrollStepInPx="50" delayInMs="16.66"/>
            </Content>
            <AppFooter />
          </Layout>
        </Layout>
      </Layout>
      </ScrollToTop>
      </Router>
    );
  }
}

export default AppLayout;
