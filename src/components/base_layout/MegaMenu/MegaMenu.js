import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
// import NavItem from './NavItem';
import './megaMenu.css';

class MegaMenu extends Component {
  getNavLink(child) {

  }
  render() {
    return (
      <nav className="navbar">
        <div className="navbar-content">
          <div className="dropdown">
              <div>
              <button className="dropbtn">
                  {this.props.name}
              </button>
              <div className="dropdown-content">
                  <div className="header">
                    {
                      this.props.child.map((item) => 
                        <NavLink to={item.path} className="nav-link">{item.name}</NavLink>
                      )
                    }
                  </div>
              </div>
              </div>
          </div>
        </div>
      </nav>
    )
  }
}

export default MegaMenu
