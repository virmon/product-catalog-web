import React, { Component } from 'react'
import { NavLink } from 'react-router-dom'
import './megaMenu.css'

export default class NavItem extends Component{
    render() {
        return(
          <div className="dropdown">
              <button className="dropbtn">
                  MegaMenu
              </button>
              <div className="dropdown-content">
                  <div className="header">
                    <NavLink to="/" className="nav-link"></NavLink>
                    <NavLink to="/" className="nav-link"></NavLink>
                    <NavLink to="/" className="nav-link"></NavLink>
                  </div>
              </div>
          </div>
        );
    }
}
