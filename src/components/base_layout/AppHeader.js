import React, { Component } from 'react';
import { Layout, Menu, Icon } from 'antd';

const { Header } = Layout;

class AppHeader extends Component {
    render() {
        return(
            <Header className="header">
                <Menu
                theme="dark"
                mode="horizontal"
                style={{ lineHeight: '64px' }}
                >
                <div className="logo">
                  <span style={{padding: '15px'}}>
                    <Icon type="book" style={{ fontSize: 20, color: '#08c' }} /> Product Catalog
                  </span>
                </div>
                </Menu>
            </Header>
        );
    }
}

export default AppHeader;
