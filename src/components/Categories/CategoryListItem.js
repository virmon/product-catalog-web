import React, { Component } from 'react';
import { Link } from "react-router-dom";
import { Card } from 'antd';
import './Category.css';
const { Meta } = Card;

class CategoryListItem extends Component {
  render() {
    return(
      <Link to={this.props.path}>
        <Card
          className="flex-item"
          hoverable
          style={{ width: 240 }}
          // cover={<img alt="example" className="img" src={this.props.img} />}
        >
          <Meta
            title={this.props.name}
            // description={this.props.desc}
          />
        </Card>
      </Link>
    );
  }
}

export default CategoryListItem;
