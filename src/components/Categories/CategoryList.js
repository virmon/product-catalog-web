import React, { Component } from 'react';
import { Input } from 'antd';
import CategoryListItem from './CategoryListItem';
import './Category.css';
const Search = Input.Search;

class CategoryList extends Component {
  constructor(props) {
    super(props);

    this.state = {
      search: '',
      cat: [
        {
          id: 1,
          name: 'Bags',
          path: '/bags'
        },
        {
          id: 2,
          name: 'Makeup',
          path: '/makeups'
        },
        {
          id: 3,
          name: 'Pens',
          path: '/pens'
        }
      ]
    }

    this.handleSearch = this.handleSearch.bind(this);
  }

  // updates search for every keypress
  handleSearch(event) {
    console.log(event.target.value);
    this.setState({
      search: event.target.value
    })
  }
  render() {
    // filter search results
    let filteredResults = this.state.cat.filter((category) => {
      var found = category.name.toLowerCase().indexOf(this.state.search.toLowerCase()) !== -1;
      return found;
    });
    return(
      <div className="">
        <Search
          placeholder="Search Item"
          onKeyUp={this.handleSearch}
        />
        <br /><br />
        <div className="flex-container">
        {
          filteredResults.map((found) =>
            <CategoryListItem key={found.id} id={found.id} name={found.name} path={found.path}/>
          )
        }
        </div>
      </div>
    );
  }
}

export default CategoryList;
