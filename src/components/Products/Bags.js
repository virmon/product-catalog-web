import React, { Component } from 'react';
import axios from 'axios';
import ProductListItem from './ProductListItem';
import { Input } from 'antd';
import './Products.css';
const Search = Input.Search;

class ProductList extends Component {
  constructor(props) {
    super(props);

    this.state = {
      search: '',
      products: []
    }

    this.handleSearch = this.handleSearch.bind(this);
  }

  // get data from REST API
  componentDidMount() {
    axios.get('http://localhost:3000/bags').then(response =>{
        console.log(response.data);
        this.setState({
          products: response.data
        })
    })
  }

  // updates search for every keypress
  handleSearch(event) {
    console.log(event.target.value);
    this.setState({
      search: event.target.value
    })
  }
  render() {
    // filter search results
    let filteredResults = this.state.products.filter((product) => {
      var found = product.name.toLowerCase().indexOf(this.state.search.toLowerCase()) !== -1;
      return found;
    });
    return(
      <div className="">
        <Search
          placeholder="Search Item"
          onKeyUp={this.handleSearch}
        />
        <br /><br />
        <div className="flex-container">
        {
          filteredResults.map((found) =>
            <ProductListItem key={found.id} id={found.id} name={found.name} cat={found.cat} img={found.img}/>
          )
        }
        </div>
      </div>
    );
  }
}

export default ProductList;
