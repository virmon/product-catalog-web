import React, { Component } from 'react';
import { Link } from "react-router-dom";
import { Card } from 'antd';
import './Products.css';
const { Meta } = Card;

class ProductListItem extends Component {
  render() {
    return(
      <Link to={'/'+this.props.cat+'/'+this.props.id}>
        <Card
          className="flex-item"
          hoverable
          style={{ width: 240 }}
          cover={<img alt="example" className="img" src={this.props.img} />}
        >
          <Meta
            title={this.props.name}
            description={this.props.cat}
          />
        </Card>
      </Link>
    );
  }
}

export default ProductListItem;
