import React, { Component } from 'react';
import axios from 'axios';
import { Button, Icon, Col, Row } from 'antd';
import ReactImageMagnify from 'react-image-magnify';
import Slider from 'react-slick';
import './Products.css';

class ProductDetail extends Component {
  constructor(props) {
    super(props);

    this.state = {
      product: []
    }

  }
  componentDidMount() {
    let pathSnippets = this.props.location.pathname.split('/');
    console.log(pathSnippets[2]);
    axios.get('http://localhost:3000/products/'+pathSnippets[2]).then(response =>{
        console.log(response.data);
        this.setState({
          product: response.data
        })
    })
  }

  render() {
    const back = e => {
      e.stopPropagation();
      window.history.back();
    };

    // Slider settings
    const settings = {
      customPaging: function(i) {
        return <a><img src={'https://os.alipayobjects.com/rmsportal/QBnOOoLaAfKPirc.png'} width='20' height='20'/></a>
        // return <a><img src={`${baseUrl}/abstract0${i+1}.jpg`}/></a>
      },
      arrows: true,
      dots: true,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1
    };

    return(
      <div>
        <Col span={32}>
          <Button type="default" onClick={back}>
            <Icon type="left" />Go back
          </Button>
        </Col>
        <div className="item-detail">
        {
          this.state.product.map((item) =>
            <div key={item.id} className="item-detail">
              <Row gutter={64}>
                <Col xs={32} sm={32} md={12} lg={12}>
                  <div className="gutter-box">
                    <Slider {...settings}>
                      <div>
                      <ReactImageMagnify {...{
                          smallImage: {
                              alt: 'sample',
                              isFluidWidth: true,
                              src: item.img,
                              srcSet: [
                                  `${item.img} 687w`,
                                  `${item.img} 770w`,
                                  `${item.img} 861w`,
                                  `${item.img} 955w`
                              ].join(', '),
                              sizes: '(min-width: 480px) 30vw, 80vw',
                          },
                          largeImage: {
                              alt: 'sample',
                              src: item.img,
                              width: 1200,
                              height: 1800
                          },
                          isHintEnabled: true,
                          shouldHideHintAfterFirstActivation: false,
                          enlargedImagePosition: 'over'
                      }} style={{zIndex:'1'}}/>
                      </div>
                    </Slider>
                  </div>
                </Col>
                <br/><br/>
                <Col xs={32} sm={32} md={12} lg={12}>
                  <div className="gutter-box">
                    <h1>{item.name}</h1>
                    <ul>
                      <li>{item.cat}</li>
                      <li>{item.img}</li>
                    </ul>
                  </div>
                </Col>
              </Row>
            </div>
          )
        }
        </div>
      </div>
    );
  }
}

export default ProductDetail;
